import requests
import base64
from datetime import datetime, date
import time
import apiPI

now = datetime.now()
formatted_date = now.strftime("%Y%m%d000000")

print("Formatted Date:", formatted_date)

req = requests.get('https://ns.atc.everynet.io/api/v1.0/history/data?access_token=f63192af95ae4f94bc96a330bcdf942a&from='+formatted_date+'&devices=000516800010c820&limit=1&type=uplink')


data = req.json()
params = data[len(data)-1]['params']
p = list(params.values())
payload = p[1]

decPayload = base64.b64decode(payload)
# print (decPayload)

strings = decPayload.decode("utf-8")
q = list(strings)

m = [int(s) for s in q if s.isdigit()]
join = ''.join(str(i) for i in m)
int_conv = int(join)
print (int_conv)

ptM_Dist = apiPI.getPiPoints('contentores_dist')[0]
print(apiPI.setValue(ptM_Dist['WebId'], int_conv))

